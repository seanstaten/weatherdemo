package dev.sean.service;

import static org.junit.Assert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import dev.sean.domain.openweather.Weather;

public class OpenWeatherServiceTest {

	@Test
	public void currentWeather() throws RestClientException {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = "http://api.openweathermap.org/data/2.5/weather?q={city}&units={units}&APPID={key}";
		ResponseEntity<Weather> response = restTemplate.getForEntity(fooResourceUrl, Weather.class,
				"Kansas City",
				"imperial",
				"0bdce20b7b01f5b99becfebc6d0474a3");
		int i = 0;
//		System.out.println(response);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
	}
}
