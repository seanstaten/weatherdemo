package dev.sean.domain.openweather;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Forecast {
	
	private final List<ForecastWeather> list;
	private final City city;

	public Forecast(@JsonProperty("list") List<ForecastWeather> list, @JsonProperty("city") City city) {
		this.list = list;
		this.city = city;
	}
	
	public List<ForecastWeather> getList() {
		return list;
	}
	
	public City getCity() {
		return city;
	}
}
