package dev.sean.domain.openweather;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Weather {

	private final long dt;
	private final String cityName;
	private final long cityId;

	private final List<Condition> conditions;
	private final MainInfo mainInfo;
	private final Wind wind;
	private final Sys sys;
	private final Coord coords;

	@JsonCreator
	public Weather(@JsonProperty("dt") long dt, @JsonProperty("name") String cityName, @JsonProperty("id") long cityId,
			@JsonProperty("main") MainInfo mainInfo, @JsonProperty("wind") Wind wind, @JsonProperty("sys") Sys sys,
			@JsonProperty("coord") Coord coords, @JsonProperty("weather") List<Condition> conditions) {
		this.dt = dt;
		this.cityName = cityName;
		this.cityId = cityId;
		this.mainInfo = mainInfo;
		this.wind = wind;
		this.sys = sys;
		this.coords = coords;
		this.conditions = conditions;
	}

	public long getDt() {
		return dt;
	}

	public String getCityName() {
		return cityName;
	}

	public long getCityId() {
		return cityId;
	}

	public MainInfo getMainInfo() {
		return mainInfo;
	}

	public Wind getWind() {
		return wind;
	}

	public Sys getSys() {
		return sys;
	}

	public Coord getCoords() {
		return coords;
	}

	public Condition getCondition() {
		if(this.conditions.size() > 0) {
			return conditions.get(0);
		}
		
		return null;
	}
}
