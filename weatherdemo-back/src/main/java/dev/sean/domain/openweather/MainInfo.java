package dev.sean.domain.openweather;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MainInfo {
	private final double temp;
	private final double pressure;
	private final int humidity;
	private final double tempMin;
	private final double tempMax;

	@JsonCreator
	public MainInfo(@JsonProperty("temp") double temp, @JsonProperty("pressure") double pressure,
			@JsonProperty("humidity") int humidity, @JsonProperty("temp_min") double tempMin,
			@JsonProperty("temp_max") double tempMax) {
		this.temp = temp;
		this.pressure = pressure;
		this.humidity = humidity;
		this.tempMin = tempMin;
		this.tempMax = tempMax;
	}

	public double getTemp() {
		return temp;
	}

	public double getPressure() {
		return pressure;
	}

	public int getHumidity() {
		return humidity;
	}

	public double getTempMin() {
		return tempMin;
	}

	public double getTempMax() {
		return tempMax;
	}

}
