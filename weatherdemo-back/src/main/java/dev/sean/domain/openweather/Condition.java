package dev.sean.domain.openweather;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Condition {

	private final int id;
	private final String name;
	private final String description;
	private final String icon;

	@JsonCreator
	public Condition(@JsonProperty("id") int id, @JsonProperty("main") String name, @JsonProperty("description") String description,
			@JsonProperty("icon") String icon) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.icon = icon;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getIcon() {
		return icon;
	}

}
