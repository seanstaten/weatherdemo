package dev.sean.domain.openweather;

import com.fasterxml.jackson.annotation.JsonProperty;

public class City {

	private final String name;
	private final String id;

	public City(@JsonProperty("name") String name, @JsonProperty("id") String id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

}
