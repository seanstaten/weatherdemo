package dev.sean.domain.openweather;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Coord {
	private final double latitude;
	private final double longitude;

	@JsonCreator
	public Coord(@JsonProperty("lat") double latitude, @JsonProperty("lon") double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

}
