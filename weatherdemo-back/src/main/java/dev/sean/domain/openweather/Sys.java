package dev.sean.domain.openweather;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Sys {
	private final long sunrise;
	private final long sunset;

	@JsonCreator
	public Sys(@JsonProperty("sunrise") long sunrise, @JsonProperty("sunset") long sunset) {
		this.sunrise = sunrise;
		this.sunset = sunset;
	}

	public long getSunrise() {
		return sunrise;
	}

	public long getSunset() {
		return sunset;
	}

}
