package dev.sean.domain.openweather;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ForecastWeather {

	private final long dt;

	private final MainInfo mainInfo;
	private final Wind wind;
	private final List<Condition> conditions;

	@JsonCreator
	public ForecastWeather(@JsonProperty("dt") long dt, @JsonProperty("main") MainInfo mainInfo,
			@JsonProperty("wind") Wind wind, @JsonProperty("weather") List<Condition> conditions) {
		this.dt = dt;
		this.mainInfo = mainInfo;
		this.wind = wind;
		this.conditions = conditions;
	}

	public long getDt() {
		return dt;
	}

	public MainInfo getMainInfo() {
		return mainInfo;
	}

	public Wind getWind() {
		return wind;
	}

	public Condition getCondition() {
		if (this.conditions.size() > 0) {
			return this.conditions.get(0);
		}

		return null;
	}

}
