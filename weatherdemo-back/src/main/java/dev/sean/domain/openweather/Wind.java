package dev.sean.domain.openweather;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Wind {
	private final double windSpeed;
	private final double windDegree;

	@JsonCreator
	public Wind(@JsonProperty("speed") double windSpeed, @JsonProperty("deg") double windDegree) {
		this.windSpeed = windSpeed;
		this.windDegree = windDegree;
	}

	public double getWindSpeed() {
		return windSpeed;
	}

	public double getWindDegree() {
		return windDegree;
	}

}
