package dev.sean.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import dev.sean.domain.openweather.Forecast;
import dev.sean.domain.openweather.Weather;

@Service
public class OpenWeatherService {
	
	@Value("${openweather.apikey}")
	String apiKey;
	
	@Autowired
	CounterService counterService;

	public Optional<Weather> weatherByCity(String cityName) {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://api.openweathermap.org/data/2.5/weather?q={city}&units={units}&APPID={apiKey}";
		ResponseEntity<Weather> response = restTemplate.getForEntity(url, Weather.class,
				cityName,
				"imperial",
				apiKey);
		
		counterService.increment(cityName);
		return response.getStatusCode() == HttpStatus.OK ? Optional.of(response.getBody()) : Optional.empty();
	}
	
	public Optional<Weather> weatherByZip(String zip) {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://api.openweathermap.org/data/2.5/weather?zip={zip}&units={units}&APPID={apiKey}";
		ResponseEntity<Weather> response = restTemplate.getForEntity(url, Weather.class,
				zip,
				"imperial",
				apiKey);
		
		counterService.increment(zip);
		return response.getStatusCode() == HttpStatus.OK ? Optional.of(response.getBody()) : Optional.empty();
	}
	
	public Optional<Forecast> forecastByCity(String cityName) {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://api.openweathermap.org/data/2.5/forecast?q={city}&units={units}&APPID={apiKey}";
		ResponseEntity<Forecast> response = restTemplate.getForEntity(url, Forecast.class,
				cityName,
				"imperial",
				apiKey);
		
		counterService.increment(cityName);
		return response.getStatusCode() == HttpStatus.OK ? Optional.of(response.getBody()) : Optional.empty();
	}
	
	public Optional<Forecast> forecastByZip(String zip) {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://api.openweathermap.org/data/2.5/forecast?zip={zip}&units={units}&APPID={apiKey}";
		ResponseEntity<Forecast> response = restTemplate.getForEntity(url, Forecast.class,
				zip,
				"imperial",
				apiKey);
		
		counterService.increment(zip);
		return response.getStatusCode() == HttpStatus.OK ? Optional.of(response.getBody()) : Optional.empty();
	}
		
}
