package dev.sean.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class CounterService {

	Map<String, LongAdder> counter = new ConcurrentHashMap<>();

	public void increment(String key) {
		counter.computeIfAbsent(key, k -> new LongAdder()).increment();
	}
	
	public Long value(String key) {
		return counter.getOrDefault(key, new LongAdder()).longValue();
	}
	
}
