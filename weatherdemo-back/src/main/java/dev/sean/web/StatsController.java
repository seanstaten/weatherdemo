package dev.sean.web;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dev.sean.service.CounterService;

@RestController
@RequestMapping("/api/1/weather/stats")
public class StatsController {
	@Autowired
	CounterService service;

	@GetMapping
	public Long get(@RequestParam Optional<String> city, @RequestParam Optional<String> zip) {
		if(city.isPresent()) {
			return service.value(city.get());
		}
		
		if(zip.isPresent()) {
			return service.value(zip.get());
		}
		
		return 0l;
	}
}
