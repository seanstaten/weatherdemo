package dev.sean.web;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dev.sean.domain.openweather.Weather;
import dev.sean.service.OpenWeatherService;

@RestController
@RequestMapping("/api/1/weather/current")
public class CurrentController {
	
	@Autowired
	OpenWeatherService service;

	@GetMapping
	public Weather get(@RequestParam Optional<String> city, @RequestParam Optional<String> zip) {
		if(city.isPresent()) {
			return service.weatherByCity(city.get()).get();
		}
		
		if(zip.isPresent()) {
			return service.weatherByZip(zip.get()).get();
		}
		
		return null;
	}
}
