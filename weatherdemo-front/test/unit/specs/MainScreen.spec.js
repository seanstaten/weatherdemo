import Vue from 'vue'
import MainScreen from '@/components/MainScreen'

describe('MainScreen.vue', () => {
  it('should see a default message above query', () => {
    const Constructor = Vue.extend(MainScreen)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('#wq-top div h1').textContent).toBe('Local Weather')
  })
})
