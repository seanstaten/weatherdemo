import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  currentLocation: ''
}

const mutations = {
  changeLocation (state, location) {
    state.currentLocation = location
  }
}

const actions = {
}

const getters = {
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
